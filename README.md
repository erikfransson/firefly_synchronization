## Firefly simulations

Run via

    python src/moving_simulation.py

or 

    python src/grid_simulation.py


## Demo
<img src="images/firefly_demo.gif" alt="drawing" width="750"/>