import os
import pygame
import numpy as np
import pandas as pd
from firefly import FireFly
from scipy.spatial.distance import cdist


class Simulation:

    background_color = pygame.Color(0, 0, 0)

    def __init__(self, window_x, window_y, ff_radius=4):
        self.step = 0
        self.window_x = window_x
        self.window_y = window_y
        self.ff_radius = ff_radius

        self.records = []

        # setup screen
        self.screen = pygame.display.set_mode((self.window_x, self.window_y), 0, 0)

    def _get_pairwise_distances(self, positions):
        distances = cdist(positions, positions)
        return distances

    def _generate_random_firefly(self):
        # generate firefly with random value and position
        value = np.random.random()
        pos = (self.window_x * np.random.random(), self.window_y * np.random.random())
        ff = FireFly(pos, value, self.ff_radius)

        # evolve firefly x number of steps in order to make active fireflies possible
        n_steps = np.random.randint(100, 500)
        for _ in range(n_steps):
            ff.evolve()
        return ff

    def loop(self, FPS=30, savedir=None, max_steps=np.inf, verbose=True):

        clock = pygame.time.Clock()

        # main loop
        running = True
        while running:
            # events
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return

            # data
            row = dict(step=self.step, value=self.average_value, intensity=self.average_intensity)
            self.records.append(row)

            if verbose:
                print('{:5d}, {:.5f}, {:.5f}'.format(self.step, self.average_value, self.average_intensity))

            # update game
            self.draw()
            self.evolve()
            clock.tick(FPS)
            pygame.display.flip()

            # save snapshot
            if savedir is not None:
                fname = os.path.join(savedir, 'state-{}.png'.format(self.step))
                pygame.image.save(self.screen, fname)

            # check for stopping
            if self.step > max_steps:
                running = False

    @property
    def data(self):
        return pd.DataFrame(self.records)

    @property
    def average_intensity(self):
        return np.mean([ff.intensity for ff in self.fire_flies])

    @property
    def average_value(self):
        return np.mean([ff.value for ff in self.fire_flies])

    def draw(self):
        # reset screen to black
        self.screen.fill(self.background_color)

        # draw flies
        for ff in self.fire_flies:
            ff.draw(self.screen)

        self._draw_graphs()

    def _draw_graphs(self):
        import matplotlib
        matplotlib.use("Agg")
        import matplotlib.backends.backend_agg as agg
        import matplotlib.pyplot as plt

        # color
        background_color = (0.1, 0.1, 0.1)
        plt.style.use('dark_background')
        plt.rcParams['axes.facecolor'] = background_color
        plt.rcParams['figure.facecolor'] = (0.04, 0.04, 0.04)

        # draw canvas
        w = 500
        h = 550

        # plot params
        alpha = 210  # transparency for figures
        color1 = (57/255, 255/255, 20/255)
        color2 = (255/255, 240/255, 0)
        lw = 2.0
        x_max = 1000
        ylim = [0, 1]

        df = self.data
        max_step = df.step.max()
        df = df[df.step >= max_step - x_max]

        xlim = [max(0, max_step-x_max), max(x_max, max_step)]

        # setup fig
        dpi = 100
        fig = plt.figure(figsize=[w/dpi, h/dpi], dpi=dpi)
        ax1 = fig.add_subplot(211)
        ax2 = fig.add_subplot(212)
        ax1.set_ylabel('Average Charge', fontsize=14)
        ax2.set_ylabel('Light Intensity', fontsize=14)

        ax1.plot(df.step, df.value, '-', color=color1, lw=lw)
        ax1.grid()
        ax2.plot(df.step, df.intensity, '-', color=color2, lw=lw)
        ax2.grid()

        for ax in [ax1, ax2]:
            ax.set_xlim(xlim)
            ax.set_ylim(ylim)
            ax.tick_params(labelsize=13)

        pos1 = [0.18, 0.58, 0.72, 0.38]
        pos2 = [0.18, 0.08, 0.72, 0.38]
        ax1.set_position(pos1)
        ax2.set_position(pos2)

        # get canvas
        canvas = agg.FigureCanvasAgg(fig)
        canvas.draw()
        renderer = canvas.get_renderer()
        raw_data = renderer.tostring_rgb()

        size = canvas.get_width_height()
        surf = pygame.image.fromstring(raw_data, size, "RGB").convert(24)
        surf.set_alpha(alpha)
        self.screen.blit(surf, (0, 0))
        plt.close()
