import pygame
import numpy as np


class FireFly:

    delta = 0.01
    intensity_drop = 0.05
    color_off = (70, 70, 70)
    color_on = (255, 240, 0)

    def __init__(self, position, value, radius):
        self.position = position
        self.value = value
        self.radius = radius
        self.intensity = 0.0
        self.is_active = False

    def evolve(self):
        if self.is_active:
            self.intensity -= self.intensity_drop
            if self.intensity <= 0:
                self.reset()
        else:
            self.value += self.delta
            if self.value >= 1:
                self.activate()

    @property
    def value(self):
        if self.is_active:
            return self.intensity
        return self._value

    @value.setter
    def value(self, v):
        v = min(1.0, v)
        v = max(0, v)
        self._value = v

    @property
    def rounded_position(self):
        return np.round(self.position).astype(int)

    @property
    def color(self):
        if self.is_active:
            x = self.intensity
            color = x * np.array(self.color_on) + (1-x) * np.array(self.color_off)
            return tuple(color)
        else:
            return self.color_off

    def reset(self):
        self.value = 0.0
        self.intensity = 0.0
        self.is_active = False

    def activate(self):
        self.value = 1.0
        self.is_active = True
        self.intensity = 1.0

    def draw(self, screen):
        pygame.draw.circle(screen, self.color, self.rounded_position, self.radius)
