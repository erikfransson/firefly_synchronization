import pygame
import numpy as np
from base_simulation import Simulation


class GridSimulation(Simulation):

    background_color = pygame.Color(0, 0, 0)

    def __init__(self, interaction, cutoff, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cutoff = cutoff
        self.interaction = interaction
        self.size_x = int(self.window_x / self.ff_radius / 2)
        self.size_y = int(self.window_y / self.ff_radius / 2)
        self.generate_random_population()
        self.generate_neighbor_list()

    def generate_neighbor_list(self):
        """ find neighbors on the grid """
        positions = [ff.position for ff in self.fire_flies]
        distances = self._get_pairwise_distances(positions)
        self.neighbor_list = []
        for i in range(len(self.fire_flies)):
            nbrs = np.where(distances[i, :] < self.cutoff)[0].tolist()
            nbrs.remove(i)
            self.neighbor_list.append(nbrs)

    def generate_random_population(self):
        fire_flies = []
        r = self.ff_radius
        for x in range(self.size_x):
            for y in range(self.size_y):
                x_pos = int((x + 0.5) * r * 2)
                y_pos = int((y + 0.5) * r * 2)
                ff = self._generate_random_firefly()
                ff.position = (x_pos, y_pos)
                ff.radius -= 1  # make fireflies non overlapping when drawing
                fire_flies.append(ff)
        self.fire_flies = fire_flies

    def evolve(self):
        # update fly values
        self.step += 1
        for ff in self.fire_flies:
            ff.evolve()

        # coupling between flies
        for ff, nbrs in zip(self.fire_flies, self.neighbor_list):
            if ff.is_active:
                continue
            if len(nbrs) == 0:
                continue

            extra_delta = self.interaction * np.sum([self.fire_flies[n].intensity==1 for n in nbrs])
            ff.value += extra_delta

    def draw(self):
        super().draw()

        # highlight a single firefly
        positions = np.array([ff.position for ff in self.fire_flies])
        d = [np.linalg.norm(p - [self.window_x//2, self.window_y//2]) for p in positions]
        index = np.argmin(d)
        if self.step == 1:
            print(len(self.neighbor_list[index]))
        ff = self.fire_flies[index]
        pygame.draw.circle(self.screen, ff.color, ff.rounded_position, self.cutoff, 2)


if __name__ == '__main__':

    # game parameters
    window_x = 1600
    window_y = 900
    ff_radius = 20
    cutoff = 150
    interaction = 0.01

    # init fire fly
    np.random.seed(42)
    pygame.init()
    sim = GridSimulation(interaction=interaction, cutoff=cutoff,
                         window_x=window_x, window_y=window_y, ff_radius=ff_radius)
    sim.loop(verbose=False)
