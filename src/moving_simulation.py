import pygame
import numpy as np
from base_simulation import Simulation


class NeighborSimulation(Simulation):

    background_color = pygame.Color(0, 0, 0)

    def __init__(self, n_flies, interaction, cutoff, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.n_flies = n_flies
        self.interaction = interaction
        self.cutoff = cutoff
        self.generate_random_population()

    def evolve(self, dt=1.0, wall=10):
        """ dt timestep, wall size (margin of bounding-box) """

        # update motion
        friction = 0.999
        a = np.random.normal(0, 0.03, (self.n_flies, 2))
        self.velocities = friction * self.velocities + dt * a
        self.positions += dt * self.velocities

        # bounce of walls
        mask_x = np.logical_or(wall > self.positions[:, 0], self.positions[:, 0] > self.window_x-wall)
        mask_y = np.logical_or(wall > self.positions[:, 1], self.positions[:, 1] > self.window_y-wall)
        self.velocities[mask_x, 0] *= -0.7
        self.velocities[mask_y, 1] *= -0.7
        self.positions[:, 0] = np.clip(self.positions[:, 0], a_min=wall, a_max=self.window_x-wall)
        self.positions[:, 1] = np.clip(self.positions[:, 1], a_min=wall, a_max=self.window_y-wall)

        # update flies
        for ff, pos in zip(self.fire_flies, self.positions):
            ff.position = pos

        # update fly values
        self.step += 1
        for ff in self.fire_flies:
            ff.evolve()

        # coupling between flies
        distances = self._get_pairwise_distances(self.positions)
        for ff, dists in zip(self.fire_flies, distances):
            if ff.is_active:
                continue
            mask = np.logical_and(1e-5 < dists, dists < self.cutoff)
            nbrs = np.where(mask)[0]
            if len(nbrs) == 0:
                continue

            extra_delta = self.interaction * np.sum([self.fire_flies[n].intensity==1 for n in nbrs])
            ff.value += extra_delta

    def draw(self):
        super().draw()

        # highlight a single firefly
        if True:
            ff = self.fire_flies[0]
            pygame.draw.circle(self.screen, ff.color, ff.rounded_position, self.cutoff, 2)

        # draw charge up for single firefly
        if False:
            ff.radius = 2 * self.ff_radius
            w, h = ff.radius, 3*ff.radius
            b = ff.radius//6
            x, y = ff.rounded_position
            x += int(ff.radius*1.1)
            y -= h/2
            h2 = (h-2*b) * ff.value
            r1 = pygame.rect.Rect(x, y, w, h)
            r2 = pygame.rect.Rect(x+b, y+h-b, w-2*b, -h2)
            pygame.draw.rect(self.screen, (200, 200, 200), r1)
            pygame.draw.rect(self.screen, (20, 220, 60), r2)

    def generate_random_population(self):
        fire_flies = []
        positions = np.random.random((self.n_flies, 2))
        velocities = np.random.normal(0, 0.1, (self.n_flies, 2))
        positions[:, 0] *= self.window_x
        positions[:, 1] *= self.window_y
        positions[0] = self.window_x//2, self.window_y//2

        for i in range(self.n_flies):
            pos = positions[i]
            ff = self._generate_random_firefly()
            ff.position = pos
            fire_flies.append(ff)
        self.fire_flies = fire_flies
        self.positions = positions
        self.velocities = velocities


if __name__ == '__main__':

    # visual parameters
    window_x = 1600
    window_y = 900
    ff_radius = 6
    FPS = 10

    # simulation parameters
    seed = 55555
    n_flies = 400
    cutoff = 100
    interaction = 0.1

    # run fire fly
    np.random.seed(seed)
    pygame.init()
    sim = NeighborSimulation(n_flies, interaction, cutoff, window_x, window_y, ff_radius)
    sim.loop(FPS=FPS, max_steps=5000, verbose=False)
